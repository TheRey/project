#!/bin/bash
HERE=$(pwd)
export LD_LIBRARY_PATH="$HERE/bin:$HERE/ruby/lib:$LD_LIBRARY_PATH"
export PATH="$HERE/ruby/bin:$PATH"
export GAMEDEPS="$HERE/bin"
./ruby/bin/ruby \
    --encoding utf-8:utf-8 \
    -I"$HERE/ruby/lib/ruby/site_ruby/3.0.0"  \
    -I"$HERE/ruby/lib/ruby/site_ruby/3.0.0/x86_64-linux"  \
    -I"$HERE/ruby/lib/ruby/site_ruby"  \
    -I"$HERE/ruby/lib/ruby/vendor_ruby/3.0.0"  \
    -I"$HERE/ruby/lib/ruby/vendor_ruby/3.0.0/x86_64-linux"  \
    -I"$HERE/ruby/lib/ruby/vendor_ruby"  \
    -I"$HERE/ruby/lib/ruby/3.0.0"  \
    -I"$HERE/ruby/lib/ruby/3.0.0/x86_64-linux" \
    -r"$HERE/bin/cleanup_loadpath" \
    Game.rb "$@"

